export interface User {
  id:number,
  nombre:string,
  apellido:string,
  email:string,
  ubicacion:string,
  telefono:string,
}
