import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { TokenStorageService } from 'src/app/services/token/token-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form : FormGroup;
  loading: boolean = false;
  isLoginFailed: boolean=false;
  isLoggedIn:boolean = false;
  errorMessage ='';
  roles:string[]=[];

  constructor(
    private authService:AuthService,
    private tokenStorage: TokenStorageService,
    private fb : FormBuilder,
    private _snackBar: MatSnackBar,
    private router:Router,
    private route: ActivatedRoute
    ) {
    this.form = fb.group({
      email: new FormControl("",[Validators.required,Validators.email]),
      password: new FormControl("",[Validators.required,Validators.minLength(8)]),
    });
  }

  ngOnInit(): void {
  }

  ingresar(){
    this.loading = true;
    if(this.form.valid){
      const {email,password} = this.form.value;

      this.authService.login(email,password).subscribe(
        res => {
          console.log(res);
          this.tokenStorage.saveToken(res.data.accessToken);
          this.tokenStorage.saveUser(res.data);

          this.isLoginFailed = false;
          this.isLoggedIn = true;

          this.roles = this.tokenStorage.getUser().roles;
          this.redirect();
        },
        err => {
          this.loading = false;
          if(err.status == 401){
            this.errorMessage = err.error.message.message;
            console.log(err.error.message.message);
          }
          this.deleteErrorMessage();
        }
      );

    }else{
      this.openSnackBar("Llene los campos correctamente");
    }

  }

  redirect(){
    this.router.navigate(['dashboard']);
  }

  deleteErrorMessage(){
    setTimeout(() => {
      this.errorMessage ='';
    }, 2000);
  }

  getTouched(){
    return this.form.touched || this.form.get('email')?.touched
    || this.form.get('password')?.touched;
  }

  email(){
    return this.form.get("email");
  }

  password(){
    return this.form.get("password");
  }

  openSnackBar(message: string){
    this._snackBar.open(message,"",{"duration": 1500, "horizontalPosition": "center","verticalPosition": "bottom", });
  }

}
