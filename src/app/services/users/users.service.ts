import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';


import { BASE_URL } from './../base_url';
import { User } from './../../interface/user';



@Injectable({
  providedIn: 'root'
})
export class UsersService {

  url:string = BASE_URL + 'users/'

  constructor(private http:HttpClient) { }

  getUsers(): Observable<any>{
    return this.http.get<any>(this.url)
    .pipe(
      catchError(this.error)
    )
  }

  error(error:HttpErrorResponse){
    let errorMessage = '';
    if(error.error instanceof ErrorEvent){
      errorMessage =error.error.message;
    }else{
      errorMessage = `Error código: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }

}
